﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickManager : MonoBehaviour
{
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);

            RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);

            if (hit.collider != null)
            {
                print(hit.collider.gameObject.name);

                TerrainControl[] allTerrain = FindObjectsOfType<TerrainControl>();

                foreach(TerrainControl t in allTerrain)
                {
                    t.isSelected = false;
                }

                TerrainControl terrainCtrl = hit.collider.gameObject.GetComponent<TerrainControl>();
                terrainCtrl.isSelected = true;
            }

            if (hit.collider == null)
            {
                TerrainControl[] allTerrain = FindObjectsOfType<TerrainControl>();

                foreach (TerrainControl t in allTerrain)
                {
                    t.isSelected = false;
                }
            }
        }
    }
}
