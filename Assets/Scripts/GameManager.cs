﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static int level = 0;

    private void Awake()
    {
        Time.timeScale = 0;    
    }

    public void NextScene()
    {
        level++;
        SceneManager.LoadScene(level);
    }

    public void SpeedTheGame()
    {
        if (Time.timeScale < 3) 
            Time.timeScale += 3;

        else Time.timeScale = 1;
    }

    
}
