﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    public static float speed;
    public static bool isGround;

    public float mySpeed;
    public float fallTime;
    
    private void Start()
    {
        speed = mySpeed;
        isGround = false;
    }

    private void Update()
    {
        transform.position = new Vector3(transform.position.x + speed * Time.deltaTime, transform.position.y);

        if (fallTime <= 0 && isGround)
        {
            print("You Died");
        }

        if (isGround) 
        {
            fallTime = 1f;
        }
        else if (!isGround)
        {
            fallTime -= Time.deltaTime;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.name == "YouLose")
        {
            Time.timeScale = 0;
            UIManager.losePanel.SetActive(true);
        }
    }
}
