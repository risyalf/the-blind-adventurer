﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    private void Start()
    {
        CheckingBGM();
    }

    void CheckingBGM()
    {
        if (FindObjectsOfType<MusicManager>().Length > 1)
        {
            Destroy(this.gameObject);
        }
    }        
}
