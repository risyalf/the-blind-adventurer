﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class PreMain : MonoBehaviour, IPointerClickHandler
{
    public void OnPointerClick(PointerEventData eventData)
    {
        if (gameObject.name == "FinishGame")
        {
            GameManager.level = 0;
            SceneManager.LoadScene(GameManager.level);
        }

        else
        {
            GameManager.level++;
            SceneManager.LoadScene(GameManager.level);
        }
    }
}
