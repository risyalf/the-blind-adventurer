﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainIconController : MonoBehaviour
{
    TerrainControl terrainCtrl;

    private void Start()
    {
        terrainCtrl = gameObject.GetComponent<TerrainControl>();

        foreach (Transform children in transform)
        {
            children.gameObject.SetActive(false);
        }
    }

    private void Update()
    {
        if (terrainCtrl.isSelected)
        {
            foreach (Transform children in transform)
            {
                if (children.name == "Icon")
                {
                    children.gameObject.SetActive(true);
                }
            }
        }

        if (terrainCtrl.isSelected == false)
        {
            foreach (Transform children in transform)
            {
                if (children.name == "Icon")
                {
                    children.gameObject.SetActive(false);
                }
            }
        }

        if (terrainCtrl.rightLimit > 0) 
        {
            foreach (Transform children in transform)
            {
                if (children.name == "RightIcon")
                {
                    children.gameObject.SetActive(true);
                }
            }
        }    
        
        else if(terrainCtrl.rightLimit <= 0)
        {
            foreach (Transform children in transform)
            {
                if (children.name == "RightIcon")
                {
                    children.gameObject.SetActive(false);
                }
            }
        }

        if (terrainCtrl.leftLimit > 0)
        {
            foreach (Transform children in transform)
            {
                if (children.name == "LeftIcon")
                {
                    children.gameObject.SetActive(true);
                }
            }
        }

        else if (terrainCtrl.leftLimit <= 0)
        {
            foreach (Transform children in transform)
            {
                if (children.name == "LeftIcon")
                {
                    children.gameObject.SetActive(false);
                }
            }
        }

        if (terrainCtrl.topLimit > 0)
        {
            foreach (Transform children in transform)
            {
                if (children.name == "UpIcon")
                {
                    children.gameObject.SetActive(true);
                }
            }
        }

        else if (terrainCtrl.topLimit <= 0)
        {
            foreach (Transform children in transform)
            {
                if (children.name == "UpIcon")
                {
                    children.gameObject.SetActive(false);
                }
            }
        }

        if (terrainCtrl.botLimit > 0)
        {
            foreach (Transform children in transform)
            {
                if (children.name == "DownIcon")
                {
                    children.gameObject.SetActive(true);
                }
            }
        }

        else if (terrainCtrl.botLimit <= 0)
        {
            foreach (Transform children in transform)
            {
                if (children.name == "DownIcon")
                {
                    children.gameObject.SetActive(false);
                }
            }
        }
    }
}
