﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastControl : MonoBehaviour
{
    public LayerMask layer;         // Untuk memilih layer mana yang terkena efek

    [Range(0,1)]
    public float dist;

    public bool hittingRight;
    public bool hittingLeft;
    public bool hittingDown;

    private void Update()
    {
        RaycastHit2D hitRight = Physics2D.Raycast(transform.position, Vector2.right, dist, layer);
        RaycastHit2D hitLeft = Physics2D.Raycast(transform.position, Vector2.left, dist, layer);
        RaycastHit2D hitDown = Physics2D.Raycast(transform.position, Vector2.down, dist, layer);

        // Right
        if (hitRight.collider != null) 
        {
            hittingRight = true;

            if (PlayerControl.isGround)
            {
                PlayerControl.speed = -1 * Mathf.Abs(PlayerControl.speed);

                FindObjectOfType<PlayerControl>().transform.rotation = Quaternion.Euler(0, 180, 0);
            }
        }

        else if (hitRight.collider == null) 
            { hittingRight = false; }
        

        // Left
        if (hitLeft.collider != null)
        {
            hittingLeft = true;

            if (PlayerControl.isGround)
            {
                PlayerControl.speed = Mathf.Abs(PlayerControl.speed);

                FindObjectOfType<PlayerControl>().transform.rotation = Quaternion.Euler(0, 0, 0);
            }
        }

        else if (hitLeft.collider == null)
        { hittingLeft = false; }


        // Down
        if (hitDown.collider != null)
        {
            hittingDown = true;

            PlayerControl.isGround = true;

            FindObjectOfType<PlayerControl>().transform.parent = hitDown.collider.gameObject.transform;

            if(gameObject.transform.parent.name == "BreakablePart")
            {
                gameObject.transform.parent = null;
            }

            if(hitDown.collider.gameObject.tag == "FinishLine")
            {
                PlayerControl.speed = 0;

                UIManager.winPanel.SetActive(true);
            }
        }

        else if (hitDown.collider == null)
        { 
            hittingDown = false;
            PlayerControl.isGround = false;

            //FindObjectOfType<PlayerControl>().transform.parent = null;
        }

    }
}
