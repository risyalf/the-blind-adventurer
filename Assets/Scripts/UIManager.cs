﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public static GameObject losePanel;
    public static GameObject winPanel;

    public GameObject theLosePanel;
    public GameObject theWinPanel;

    private void Awake()
    {
        losePanel = theLosePanel;
        winPanel = theWinPanel;
    }

    private void Start()
    {
        losePanel.SetActive(false);
        winPanel.SetActive(false);
    }

    public void NextLevel()
    {
        losePanel.SetActive(false);
        winPanel.SetActive(false);

        GameManager.level++;
        SceneManager.LoadScene(GameManager.level);
        
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene(GameManager.level);
    }

    public void ToMainMenu()
    {
        GameManager.level = 0;
        SceneManager.LoadScene(GameManager.level);
    }
}
